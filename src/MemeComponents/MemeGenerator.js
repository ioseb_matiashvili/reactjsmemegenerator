import React, {Component} from 'react';

class MemeGenerator extends Component {
    constructor() {
        super();
        this.state = {
            topText: "",
            buttomText: "",
            randomImg: "http://i.imgflip.com/1bij.jpg",
            allMemeImgs: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        this.setState({ [name]: value });
    }

    // API call 
    componentDidMount() {
        fetch("https://api.imgflip.com/get_memes")
            .then(response => response.json())
            .then(response => {
                const {memes} = response.data
                console.log(memes[0])
                this.setState({allMemeImgs: memes})
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const randomNum =  Math.floor(Math.random() * this.state.allMemeImgs.length);
        const randomMemeImg = this.state.allMemeImgs[randomNum].url; 
        this.setState({
            randomImg: randomMemeImg
        })
    }
    
    render() {
        return (
            <div>
                <form className="meme-form" onSubmit={this.handleSubmit}>                 
                    <input 
                        type="text" 
                        name="topText" 
                        placeholder="Top Text"
                        value={this.state.toptext}
                        onChange={this.handleChange}
                    />
                    <input 
                        type="text" 
                        name="buttomText" 
                        placeholder="Bottom Text"
                        value={this.state.buttomText}
                        onChange={this.handleChange}
                    />
                    <button>Generate</button>
                </form>

                <div className="meme">
                    <img src={this.state.randomImg} alt="" />
                    <h2 className="top">{this.state.topText}</h2>
                    <h2 className="buttom">{this.state.buttomText}</h2> 
                </div>
            </div>
        )
    }
}

export default MemeGenerator;