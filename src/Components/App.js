import React from 'react';
import TodoItem from './TodoItem'
import todosData from './todosData';
import '../App.css';

class App extends React.Component {
    constructor () {
        super();
        this.state = {
            loading: false,
            character: {}
        }
    }

    componentDidMount() {
        this.setState({loading: true})
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loading: false,
                    character: data
                })
            })
    }

    render() {
        const text = this.state.loading ? "Loading.." :this.state.character.body;
        return (
            <div>
                {text}
            </div>
        );
    }
}

export default App;