import React from 'react';

function Product(props) {
    console.log(props);
    return (
        <div>
            <h2>{props.product.name}</h2>
            <h5>{props.product.price}</h5>
            <h5>{props.product.description}</h5>
        </div>
    );
}

export default Product;