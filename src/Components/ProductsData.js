

const products = [
    {
        id: "1",
        name: "Pencil",
        price: 2,
        description: "Just for writing"
    },
    {
        id: "2",
        name: "Pen",
        price: 3,
        description: "Just for writing"
    },
    {
        id: "3",
        name: "Ball",
        price: 7,
        description: "Just for writing"
    },
    {
        id: "4",
        name: "Rubber",
        price: 1,
        description: "Just for writing"
    },
    {
        id: "5",
        name: "Cotton",
        price: 0.5,
        description: "Just for writing"
    }
]

export default products;